
public class Main {

	public static void main(String[] args) {
		Car car1 = new Car();
		car1.display();
		Car car2 = new Car("Red");
		car2.display();
		Car car3 = new Car("Racing Green" , "Aston Martin");
		car3.drive(23, 78);
		car3.display();
		car1.setOdometer(100000);
		car2.incrementGear();
		car2.display();
	
	}

}
