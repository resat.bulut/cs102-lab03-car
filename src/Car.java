
public class Car {
	private double odometer;
	private String brand;
	private String colour;
	private int gear;
	private double totalDriveTime;
	
	public Car()
	{
		odometer = 0;
		colour = "Grey";
		brand = "Fiat";
		gear = 0;
		
	}
	public Car(String c) 
	{
		odometer = 0;
		colour = c;
		brand = "Fiat";
		gear = 0;
	}
	
	public Car(String c, String b)
	{
		odometer = 0;
		colour = c;
		brand = b;
		gear = 0;
	}
	
	public void incrementGear()
	{
		if (gear < 5)
		{
			this.gear += 1;
		}else
		{
			System.out.println("Gear must be in the range of [0 , 5]");
		}
	}
	
	public void decrementGear()
	{
		if (gear > 0) 
		{
			this.gear -= 1;
		}else
		{
			System.out.println("Gear must be in the range of [0 , 5]");
		}
	}
	
	public void drive(double driveTime, double kmTravelledHour) 
	{
		totalDriveTime += driveTime;
		odometer += totalDriveTime * kmTravelledHour;
		
	}
	
	public double getOdometer()
	{
		return odometer;
	}
	
	public void setOdometer(double odo) 
	{
		odometer = odo;
	}
	
	public String getBrand()
	{
		return brand;
	}
	
	public void setBrand(String b)
	{
		brand = b;
	}
	
	public String getColour()
	{
		return colour;
	}
	
	public void setColour(String c)
	{
		colour = c;
	}
	
	public int getGear()
	{
		return gear;
	}
	
	public void display()
	{
		System.out.println("Car brand: " + brand + "		Car colour: " + colour + "		Km: " + odometer +
							"		Current gear: " + gear + "		Total drive time: " + totalDriveTime	);
	}
	public double getAvSpeed()
	{
		return odometer / totalDriveTime;
	}
}
